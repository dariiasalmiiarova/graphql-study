const fetch = require('node-fetch')
const util = require('util')
const { GraphQLSchema, GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } = require('graphql')
const parseXML = util.promisify(require('xml2js').parseString) //returns a promise instead of callbacks

// fetch(
//     'https://www.goodreads.com/author/show/18541?format=xml?id=4432&key=5t0AlbDb9NJSDkAaJBRgrg'
// ).then(response => response.text()) //text bcs it's xml
// .then(parseXML)

const BookType = new GraphQLObjectType({
    name: 'Book',
    decription: '...',

    fields: () => ({
        title: {
            type: GraphQLString,
            resolve: xml => 
                xml.title[0]
        },
        isbn: {
            type: GraphQLString,
            resolve: xml => 
                xml.isbn[0]
                // console.log('this is isbn: ', xml.isbn[0])
        }
    })
})

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    decription: '...',

    fields: () => ({
        name: {
            type: GraphQLString,
            //In resolve function we tell the schema where the data, that we're needing are.
            resolve: xml => {
                xml.GoodreadsResponse.author[0].name[0]
                // console.log('the name: ', xml.GoodreadsResponse.author[0].name[0])
            }
        },
        books: {
            type: new GraphQLList(BookType),
            resolve: xml => 
                xml.GoodreadsResponse.author[0].books[0].book
        }
    })
})

module.exports = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Query',
        description: '...',

        fields: () => ({
            author: {
                type: AuthorType,
                args:{
                    id: { type: GraphQLInt }
                },
                resolve: (root, args) => fetch(
                        `https://www.goodreads.com/author/show.xml?id=${args.id}&key=5t0AlbDb9NJSDkAaJBRgrg`
                    ).then(response => response.text()) //text bcs it's xml
                    .then(parseXML) 
            }
        })
    })
})