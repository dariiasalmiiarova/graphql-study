const express = require('express')
const { graphqlHTTP } = require('express-graphql')
// const { GraphQLSchema, GraphQLObjectType, GraphQLInt, GraphQLString, GraphQLList } = require('graphql')
const app = express()

const schema = require('./schema')

// const schema = new GraphQLSchema({
//     query: new GraphQLObjectType({
//         name: 'HelloWorld',
//         fields: () => ({
//             message: { 
//                 type: GraphQLString,
//                 resolve: () => 'Hello World'
//             }
//         })
//     })
// })

//GraphQl endpoint
app.use('/graphql', graphqlHTTP({
    schema: schema,
    graphiql:true
}))

app.listen(4000., () => console.log('Listening...'))
